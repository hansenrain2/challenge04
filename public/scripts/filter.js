class Car {
    filterCarByUser() {
        var driver = document.getElementById("driver").value;
        var date = document.getElementById('date').value;
        var time = document.getElementById('time').value;
        var dateTime = date + time;
        var passanger = document.getElementById('passanger').value;

        if (driver === undefined || driver === "") {
            alert("Tipe Driver harus dipilih!");
            return;
        } else if (dateTime < getDateTimeNow()) {
            alert("Pilih tanggal dan jam yang diatas sekarang!");
            return;
        } else if (passanger == "" && driver != "") {
            return this.cars.filter(car => car.available === true && car.availableAt <= dateTime);
        } else if (passanger != "" && driver != "") {
            return this.cars.filter(car => car.available === true && car.capacity >= passanger && car.availableAt <= dateTime);
        }
    }
    constructor(cars) {
        this.cars = cars;
    }
    filterCarAvailable() {
        return this.cars.filter(car => car.available === true);
    }
}

var xmlHttp = new XMLHttpRequest();
xmlHttp.open("GET", "http://localhost:8000/api/cars", false);
xmlHttp.send(null);

var data = JSON.parse(xmlHttp.responseText);
var cars = new Car(data);
var app = document.getElementById('carsList');
htmlData = "";
data = cars.filterCarAvailable();

function rupiah(number) {
    return new Intl.NumberFormat("id-ID", {style:"currency", currency: "IDR"}).format(number);
}

var btnFilter = document.getElementById('btnFilter').addEventListener('click', getCars);

function getDateTimeNow() {
    var today = new Date();
    var date = today.getFullYear()+'-'+String((today.getMonth()+1)).padStart(2, '0')+'-'+String(today.getDate()).padStart(2, '0');
    var time = String(today.getHours()).padStart(2, '0') + ":" + String(today.getMinutes()).padStart(2, '0') + ":" + String(today.getSeconds()).padStart(2, '0');
    var dateNow = date+'T'+time+'.000Z';
    return dateNow;
}

function getCars() {
    var htmlData = "";
    data=cars.filterCarByUser();
    if (data==="" || data===undefined) {
        htmlData="";
        app.innerHTML = htmlData;
        return;
    } else {
        for (let index = 0; index < data.length; index++) {
            var car = data[index];
            var rentCost = rupiah(car.rentPerDay);
            htmlData += `
            <div class="col m-2">
                <div class="card" style="width: 18rem; height: 550px">
                <img src="${car.image}"" class="card-img-top img-fluid" alt="${car.manufacture}" style="height: 190px; object-fit: scale-down;">
                <div class="card-body" style="font-size: 14px;">
                    <p class="card-title">${car.manufacture} ${car.model}</p>
                    <p class="fw-bold">${rentCost} / hari</p>
                    <p class="card-text" style="height: 90px">${car.description}</p>
                    <div class="my-2"><i class="bi bi-people me-2"></i>${car.capacity} Orang</div>
                    <div class="my-2"><i class="bi bi-gear me-2"></i>${car.transmission}</div>
                    <div class="my-2"><i class="bi bi-calendar4 me-2"></i>${car.year}</div>
                    <a href="#" class="btn bg-button text-white w-100 mt-2 fw-bold mt-4" style="font-size: 14px;">Pilih Mobil</a>
                </div>
                </div>
            </div>
            `;
        }
        app.innerHTML = htmlData;
        if (htmlData=="") {
            alert("No car available");
        }
    }
}